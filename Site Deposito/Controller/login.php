<?php

    class Login{

        /**  @var object Conexão com o banco de dados*/
        private $db_connection = null;

        /**  @var array Mensasgens de erro*/
        public $errors = array();

        /** @var array Mensagens sucess e neutral*/
        public $messages = array();

        /** @var boolean Identifica se o login deu errado*/
        public $login_error = false;

        public function __construct(){
            //criando a sessão
            session_start();

            if (isset($_GET["logout"])){
                $this->logout();
            }
            elseif(isset($_POST["login"])){
                $this->login();
            }
        }

        private function login(){

            $user = $_POST['Usuario'];
            $pswd = $_POST['Senha'];
            $logado = false;

            /*if (empty($user))
                $this->errors[] = "Campo usuario esta vazio";
            elseif (empty($pswd))
                $this->errors[] = "Campo senha esta vazio";
            elseif ((!empty($user)) && (!empty($pswd)) ){}*/

            require_once('conexao.php');

            //$sql = 'SELECT REFERENCIAL, NOME, RAZAO_SOCIAL, SENHA FROM fin_clientes WHERE REFERENCIAL =' . $user;
            $sql = 'SELECT REFERENCIAL, NOME, RAZAO_SOCIAL FROM fin_clientes WHERE REFERENCIAL =' . $user . 'AND SENHA = ' . $pswd;
            $rc = ibase_query($dbh, $sql);

                /*if (ibase_fetch_object($rc)){
                    $this->messages[] = 'Tem dados';
                }else{
                    $this->messages[] = 'Não tem dados';
                }*/
        
                while ($row = ibase_fetch_object($rc)){

                    $usuario = new StdClass();
                
                    $usuario->referencial = $row->REFERENCIAL;
                    $usuario->nome = iconv('UTF-8', 'UTF-8//IGNORE', utf8_encode($row->NOME));
                    $usuario->razaoSocial = utf8_encode($row->RAZAO_SOCIAL);
                    $_SESSION['usuario'] = $usuario;
                    $_SESSION['user_login_status'] = 1;

                    $logado = true;
                }

                if (!$logado)
                    $this->login_error = true;
   
                
        }

        private function logout(){
            $_SESSION = array();   
            session_destroy();
            $this->messages[] = "Você saiu do sistema";
        }

        public function isUserLoggedIn(){
            if (isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] == 1)
                return true;
                
            return false;
        }
    }
?>