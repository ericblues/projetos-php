<?php
    require_once('conexao.php');
    $refCliente = $_GET['refCliente'];

    $sql = 'SELECT REFERENCIAL, REF_SAIDA, DATA, VALOR FROM FIN_RECEBER WHERE REF_CLIENTE = '.$refCliente.' AND STATUS IS NULL AND VALOR_RECEBIDO IS NULL';
    $rc = ibase_query($dbh, $sql);


   
    $compras = array();

    while ($row = ibase_fetch_object($rc)){

        $compra = new StdClass();
        $compra->referencial = $row->REFERENCIAL;
        $compra->numero = $row->REF_SAIDA;
        $compra->data = str_replace("-", "/", date_format(date_create($row->DATA), "d-m-Y"));
        $compra->total = number_format($row->VALOR, 2, ",", ".");

        array_push($compras, json_encode($compra)); 
    }

    echo json_encode($compras);

    
?>