<?php
    require_once('conexao.php');

    //referencial rececbido como parametro pelo post
    $ref_saida = $_REQUEST['codCompra'];

    //Consulta SQL para tabela VEN_SAIDA_PRODUTOS
    $sql = 'SELECT NOME, UNITARIO, QUANTIDADE, VALOR FROM VEN_SAIDA_PRODUTOS WHERE REF_SAIDA = ' . $ref_saida;
    //Consulta SQL para tabela MOR__SAIDA_PRODUTOS
    $sqlMor = 'SELECT NOME, UNITARIO, QUANTIDADE, VALOR FROM MOR_SAIDA_PRODUTOS WHERE REF_SAIDA = ' . $ref_saida;

    //Resultado da Query
    $rq = ibase_query($dbh, $sql);

    $produtos = array();
    $mensagem = new StdClass();


    while ($row = ibase_fetch_object($rq)){
        $produto = new StdClass();

        $produto->nome = iconv('UTF-8', 'UTF-8//IGNORE', utf8_encode($row->NOME));
        $produto->preco = number_format($row->UNITARIO, 2, ",", ".");
        $produto->quantidade = $row->QUANTIDADE;
        $produto->total = number_format($row->VALOR, 2, ",", ".");

        array_push($produtos, json_encode($produto)); 
    }

    if (!empty($produtos)){
        echo json_encode($produtos);
    }else{
        $rq = ibase_query($dbh, $sqlMor);

        while ($row = ibase_fetch_object($rq)){
            $produto = new StdClass();
            
            $produto->nome = iconv('UTF-8', 'UTF-8//IGNORE', utf8_encode($row->NOME));
            $produto->preco =number_format($row->UNITARIO, 2, ",", ".");
            $produto->quantidade = $row->QUANTIDADE;
            $produto->total = number_format($row->VALOR, 2, ",", ".");
    
            array_push($produtos, json_encode($produto)); 
        }

        if (!empty($produtos)){
            echo json_encode($produtos);
        }else{
            $mensagem->titulo = 'Resposta';
            $mensagem->texto = 'Nao existem produtos para listar';

            echo json_encode($mensagem);
        }

        
    }

    

?>