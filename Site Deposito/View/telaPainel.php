<!DOCTYPE html>
<html>
  <head>
    <title>Depósito Tião Silva</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="./View/css/w3.css">
    <link rel="stylesheet" href="./View/css/painel.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    

    <link rel="apple-touch-icon" sizes="180x180" href="./View/image/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="./View/image/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="./View/image/favicon-16x16.png">
    <link rel="manifest" href="./View/image/manifest.json">
    <link rel="mask-icon" href="./View/image/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="./View/image/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="./View/image/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
  </head>
  
  <!--<link rel="shortcut icon" href="" type="image/x-icon" />-->
  <body class="w3-light-grey" onload="abrePagina('View/listaCompras.php')">
    <!-- Top container -->
    <div class="w3-bar w3-top w3-black w3-large" style="z-index:4">
      <button class="w3-bar-item w3-button w3-hide-large w3-hover-none w3-hover-text-light-grey" onclick="w3_open();"><i class="fa fa-bars"></i>  Menu</button>
      <span class="w3-bar-item w3-right">Depósito Tião Silva</span>
    </div>

    <!-- Sidebar/menu -->
    <nav class="w3-sidebar w3-collapse w3-white w3-animate-left" style="z-index:3;width:250px;" id="mySidebar"><br>
      <div class="w3-container w3-row">
        <div class="w3-col s4">
          <img src="./View/image/avatar2.png" class="w3-circle w3-margin-right" style="width:46px">
        </div>
        <div class="w3-col s8 w3-bar">
          <div id="spanUsuario">
            <?php
              $usuario = new StdClass();
              $usuario = $_SESSION['usuario'];
              echo "Olá $usuario->razaoSocial";
            ?>
          </div>
        </div>
      </div>
      <hr>
      <div class="w3-container">
        <h5>Painel</h5>
      </div>
      <div class="w3-bar-block">
        <a href="#" class="w3-bar-item w3-button w3-padding-16 w3-hide-large w3-dark-grey w3-hover-black" onclick="w3_close()" title="close menu"><i class="fa fa-remove fa-fw"></i>  Fechar Menu</a>
        <a href="javascript: abrePagina('View/listaCompras.php')" class="w3-bar-item w3-button w3-padding"><i class="fa fa-eye fa-fw"></i>Consulta</a>
      
        <a href="javascript: abrePagina('View/contato.html')" class="w3-bar-item w3-button w3-padding"><i class="fa fa-users fa-fw"></i>Duvidas e Contatos</a>
        <a href="index.php?logout" class="w3-bar-item w3-button w3-padding"><i class="fa fa-sign-out"></i>Sair</a><br><br>
      </div>
    </nav>

    <!-- Overlay effect when opening sidebar on small screens -->
    <div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

    <!-- !PAGE CONTENT! -->
    <div class="w3-main" style="margin-left:250px;margin-top:43px;">
      <section id="conteudo"></section>                       
    </div>
    <br>
    <script src="./View/javascript/sidebar.js"></script>
    <script src="./View/javascript/tabela.js"></script>
    <script>
      function abrePagina(caminho){

      document.getElementById("conteudo").innerHTML="";
      var alturaFrame = window.innerHeight -3;

      document.getElementById("conteudo").innerHTML="<iframe frameborder='0' src='"+caminho+"' class='w3-animate-left' height='"+alturaFrame+"px' width='100%'></iframe>";
      }

    </script>
 
  </body>
</html>
