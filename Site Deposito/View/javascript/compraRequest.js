(function(){

    console.log('Função Auto Executavel')
    const xmlhttp = new XMLHttpRequest()

    xmlhttp.onreadystatechange = function(){
        if (this.readyState === 4 && this.status == 200){
            console.log('Dados do usuario: ' + this.responseText)

            let usuario = JSON.parse(this.responseText)
            consultaCompras(usuario.referencial)
        }
    }

    xmlhttp.open("GET", "../Controller/dadosUsuario.php")
    xmlhttp.send()
})();

function consultaCompras(refCliente){

    const xmlhttp = new XMLHttpRequest()

    xmlhttp.onreadystatechange = function(){
        if (this.readyState === 4 && this.status == 200){
            //console.log('Compras: ' + this.responseText)

            preencherLista(this.responseText, refCliente)
        }
    }

    xmlhttp.open("GET", "../Controller/consultaCompra.php?refCliente=" + refCliente, true)
    xmlhttp.send()
}

function consultaTotal(refCliente){

    const xmlhttp = new XMLHttpRequest()
    let txtTotal = document.getElementById('txtTotal')

    xmlhttp.onreadystatechange = function(){
        if (this.readyState === 4 && this.status == 200){
            console.log('Total: ' + this.responseText)

            txtTotal.innerHTML = `<b>Saldo Devedor: ${this.responseText}<b>`
        }
    }

    xmlhttp.open("GET", "../Controller/consultaTotal.php?refCliente=" + refCliente, true)
    xmlhttp.send()
}

function consultaProduto(codCompra){

    if (codCompra == null)
        console.log("o id da compra é nulo")
    else{
        let tab = document.getElementById(codCompra)
    
        if (tab.children.length > 0)
            console.log('A tabela já possui elementos')
        else{
            console.log('A tabela ainda não possui elementos')
        
            let xmlhttp = new XMLHttpRequest()

            console.log(codCompra)

            xmlhttp.onreadystatechange = function (){
                if (this.readyState === 4 && this.status == 200){
                    console.log('Resposta: ' + this.responseText)

                    let json = JSON.parse(this.responseText)
                    if (json.hasOwnProperty("titulo")){
                        console.log(json.titulo)
                        console.log(json.texto)
                    }else{
                        preencherTabela(this.responseText, codCompra)
                    }
                }
            }

            xmlhttp.open("GET", "../Controller/consultaProduto.php?codCompra=" + codCompra, true)
            xmlhttp.send()
        }
    }
    
}

const preencherTabela = function(produtos, refCompra){
    const tabela = document.getElementById(refCompra)

    const objProdutos = JSON.parse(produtos)

    let tr = document.createElement('tr')
    let thProduto = document.createElement('th')
    let thPreco = document.createElement('th')
    let thQtde = document.createElement('th')
    let thTotal = document.createElement('th')

    let Produto = document.createTextNode('Produto')
    let Preco = document.createTextNode('Preco')
    let Qtde = document.createTextNode('Qtde')
    let Total = document.createTextNode('Total')

    thProduto.appendChild(Produto)
    thPreco.appendChild(Preco)
    thQtde.appendChild(Qtde)
    thTotal.appendChild(Total)

    tr.appendChild(thProduto)
    tr.appendChild(thPreco)
    tr.appendChild(thQtde)
    tr.appendChild(thTotal)

    tabela.appendChild(tr)

    for(i in objProdutos){
        let produto = JSON.parse(objProdutos[i])
        //console.log(objProduto.nome)
        
        let row = tabela.insertRow()
        let row_produto = row.insertCell(0)
        let row_preco = row.insertCell(1)
        let row_qtde = row.insertCell(2)
        let row_total = row.insertCell(3)

        row_produto.innerHTML = produto.nome
        row_preco.innerHTML = produto.preco
        row_qtde.innerHTML = produto.quantidade
        row_total.innerHTML = produto.total

    }
}

function preencherLista(jsonCompras, refCliente){
    const lista = document.getElementById('listaCompras')

    const compras = JSON.parse(jsonCompras)

    let modal = document.getElementById('modal')
    let aviso = document.getElementById('aviso01')



    console.log(modal)

    for (i in compras){
  
        let compra = JSON.parse(compras[i])
        let li = document.createElement("li")
        let ref_saida = compra.numero === null ? 'Sem vinculo' : compra.numero
        
    

        li.innerHTML = `
                <div class="grid-container accordion" onclick="consultaProduto(${compra.numero})">
                    <div>${ref_saida}</div>
                    <div>${compra.data}</div>
                    <div>${compra.total}</div>
                </div>
  
                <div class="panel grid-container-detalhes">
                    <div class="area-tabela" style="overflow-x:auto;">
                        <table id="${compra.numero}" class="w3-table w3-striped w3-padding-32 w3-bordered">
                            
                        </table>
                    </div>
                </div>`
        lista.appendChild(li)
    }
    mostrarDetalhes()
    modal.style.display = "none"
    if (compras.length === 0)
        aviso.style.display = 'block'

    consultaTotal(refCliente);
}
  
const mostrarDetalhes = function(){

    let acc = document.getElementsByClassName("accordion"); 
    let i;
    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
        /* Toggle between adding and removing the "active" class,
        to highlight the button that controls the panel */
        this.classList.toggle("active")

        /* Toggle between hiding and showing the active panel */
        let panel = this.nextElementSibling;
        if (panel.style.display === "grid") {
            panel.style.display = "none"
        } else {
            panel.style.display = "grid"
        }
        });
    } 
}


