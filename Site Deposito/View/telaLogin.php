<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="View/css/login.css">
    <link rel="stylesheet" href="View/css/w3.css">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="apple-touch-icon" sizes="180x180" href="./View/image/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="./View/image/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="./View/image/favicon-16x16.png">
    <link rel="manifest" href="./View/image/manifest.json">
    <link rel="mask-icon" href="./View/image/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="./View/image/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="./View/image/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <title>Login</title>
</head>
<body>
    <div class="grid-container">
        <div></div>   
            <div class="grid-container-form">
                <div>
                    <h3 style="text-align: center">Depósito Tião Silva</h3>
                </div>
                <div>
                    <form action="./index.php" method="POST">
                        <?php if (isset($login) && $login->login_error): ?>
                        <div id="spanErro" style="text-align: right; color: red" class="panel">
                            <span>*Usuário ou senha incorretos</span><br>
                            <span>Solicite sua senha junto à empresa</span>
                        </div>
                        <?php endif?>
                        <b><label for="Usuario">Usuário</label></b>
                        <input type="text" name="Usuario" id="user" required>
                        <b><label for="Senha">Senha</label></b>
                        <input type="password" name="Senha" id="pswd" required>
                        <input id="btnLogin" type="submit" value="Entrar" name="login">
                    </form>
                </div>
                <div style="background-color:#f1f1f1">
                    <span id="esq01">Esqueceu sua senha?</span>
                </div>
            </div>
        <div></div>
    </div>

    <div id="avisoSenha" class="w3-modal">
        <div class="w3-modal-content">
            <div class="w3-container">
                <span 
                    onclick="document.getElementById('avisoSenha').style.display='none'"
                    class="w3-button w3-display-topright">&times;
                </span>
                <h5><b>Instruções: </b></h5>
                <p>Caso você tenha esquecido sua senha, ou número de usuário, ou ainda não possui senha de acesso ao sistema,
                    entre em contao conosco através dos telefones: 
                </p>
                <ul>
                    <li>telefone do cliente</li>
                     <li>telefone do cliente</li>
                </ul>
                <p>Atenciosamente, <b>nome do cliente</b></p>
                
            </div>
        </div>
    </div>

    <script>

        let esqSenha = document.getElementById('esq01')
        esqSenha.addEventListener("click", function(){
            console.log('Função')
            let modal = document.getElementById('avisoSenha')
            modal.style.display = 'block'
        })
    </script>
</body>
</html>
