<link rel="stylesheet" href="css/table.css">
<link rel="stylesheet" href="css/w3.css">
<link rel="stylesheet" href="css/loader.css">
<header class="w3-container" style="padding-top:22px">
    <h5><b><i class="fa fa-dashboard"></i> Minhas Compras</b></h5>
    <h5 id="txtTotal"><b><i class="fa fa-dashboard"></i> Saldo Devedor: </b></h5> 
    </div>
</header>  
<br>
<div id="modal">
    <div id="loader"></div>
</div>

<div id="aviso01" class="w3-modal">
    <div class="w3-modal-content">
        <div class="w3-container">
            <span 
                onclick="document.getElementById('aviso01').style.display='none'"
                class="w3-button w3-display-topright">&times;
            </span>
            <h5><b>Nenhum pedido aberto</b></h5>
            <p>Você não possui pedido em aberto cadadstrado no momento.</p>
            <p>O sistema é atualizado todos os dias depois das 18 horas, se você possui algum
                pedido que ainda não esteja listado, por favor verifique novamente após esse horário.
            </p>
        </div>
    </div>
</div>

<div class="w3-container">
    <ul id="listaCompras" class="w3-ul w3-border">
        <li>
            <div class="grid-container">
                <div><b>Pedido</b></div>
                <div><b>Data Compra</b></div>
                <div><b>Total R$</b></div>
            </div>
        </li>
    </ul>
</div>

<script src="javascript/compraRequest.js"></script>