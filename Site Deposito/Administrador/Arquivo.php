<?php
    require_once('Classes/Cliente.php');
    require_once('Classes/Receber.php');
    require_once('Classes/Produto.php');
    
    class Arquivo{

        private $arrayClientes = array();    
        private $arrayReceber = array();   
        private $arrayProdutos = array();

        public function lerCsvClientes(){
            $caminhoCli = "caminho do csv";
            $clientesCSV = file($caminhoCli);
            $arrayClientes = array();
            foreach($clientesCSV as $i => $cli){
                list($referencial, $razaoSocial, $senha) = explode(";", $cli);
                $cliente = new Cliente;
                $cliente->setReferencial(trim($referencial));
                $cliente->setRazaoSocial(trim($razaoSocial));
                $cliente->setSenha(trim($senha));
        
                array_push($arrayClientes, $cliente);
            }

            return $arrayClientes;
        }

        public function lerCsvReceber(){
            $caminhoRec = "caminho do csv";
            $receberCSV = file($caminhoRec);
            $arrayReceber = array();

            foreach($receberCSV as $i => $rec){
                list($referencial, $ref_cliente, $ref_saida, $data, $valor) = explode(";", $rec);
        
                $receber = new Receber;
                $receber->setReferencial(trim($referencial));
                $receber->setRefCliente(trim($ref_cliente));
                $receber->setRefSaida(trim($ref_saida));
                $receber->setData(trim($data));
                $receber->setValor(trim($valor));
        
                array_push($arrayReceber, $receber);
            }

            return $arrayReceber;
        }

        public function lerCsvProdutos(){
            $caminhoPro = "caminho do csv";
            $produtosCSV = file($caminhoPro);
            $arrayProdutos = array();

            foreach($produtosCSV as $i => $prod){
                list($referencial, $ref_saida, $nome, $unitario, $quantidade, $valor) = explode(";", $prod);
        
                $produto = new Produto;
                $produto->setReferencial($referencial);
                $produto->setRefSaida($ref_saida);
                $produto->setNome($nome);
                $produto->setUnitario($unitario);
                $produto->setQuantidade($quantidade);
                $produto->setValor($valor);
        
                array_push($arrayProdutos, $produto);
            }

            return $arrayProdutos;
        }
    }     
?>
