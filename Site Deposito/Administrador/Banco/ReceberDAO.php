<?php
    Class ReceberDAO{
        public static function deleteReceber(){
            require('conexao.php');
            $status = false;

            $sql = 'DELETE FROM FIN_RECEBER';

            $statement = ibase_prepare($dbh, $sql); 

            if (ibase_execute($statement))
                $status = true;
        
            ibase_free_query($statement);

            return $status;
            
        }

        public static function insertReceber($receber){
            require('conexao.php');
            $status = true;

            $sql = 'INSERT INTO FIN_RECEBER
                        (REFERENCIAL,
                        REF_CLIENTE,
                        REF_SAIDA,
                        DATA,
                        VALOR)
                    VALUES (?, ?, ?, ?, ?)';

            $statement = ibase_prepare($dbh, $sql);

            foreach ($receber as $rec){
                $resultado = ibase_execute($statement,
                    $rec->getReferencial(),
                    $rec->getRefCliente(),
                    $rec->getRefSaida(),
                    $rec->getData(),
                    $rec->getValor());

                if (resultado == false)
                    $status = false;
            }
            ibase_free_query($statement);

            return $status;

        }
    }
?>