<?php
    Class ClienteDAO {

        public static function deleteClientes(){
            require('conexao.php');
            $status = false;

            $sql = 'DELETE FROM FIN_CLIENTES';

            $statement = ibase_prepare($dbh, $sql);

            if (ibase_execute($statement))
                $status = true;
        
            ibase_free_query($statement);

            return $status;

        }
        
        public static function insertClientes($clientes){
            require('conexao.php');
            $status = true;
            $sql = 'INSERT INTO FIN_CLIENTES
                        (REFERENCIAL,
                        RAZAO_SOCIAL,
                        SENHA) 
                    VALUES (?, ?, ?)';

            $statement = ibase_prepare($dbh, $sql);

            foreach ($clientes as $cliente){

                $resultado = ibase_execute($statement, 
                    $cliente->getReferencial(),
                    $cliente->getRazaoSocial(),
                    $cliente->getSenha());

                if ($resultado == false)
                    $status = false;
            }
            ibase_free_query($statement);

            return $status;
            
        }

        
    }
?>