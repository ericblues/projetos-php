<?php
    Class ProdutoDAO{
        public static function deleteProdutos(){
            require('conexao.php');
            $status = false;

            $sql = 'DELETE FROM VEN_SAIDA_PRODUTOS';

            $statement = ibase_prepare($dbh, $sql);

            if (ibase_execute($statement))
                $status = true;
        
            ibase_free_query($statement);

            return $status;
            
        }

        public static function insertProdutos($produtos){
            require('conexao.php');
            $status = true;

            $sql = 'INSERT INTO VEN_SAIDA_PRODUTOS
                        (REFERENCIAL,
                        REF_SAIDA,
                        NOME,
                        UNITARIO,
                        QUANTIDADE,
                        VALOR) 
                    VALUES (?, ?, ?, ?, ?, ?)';

            $statement = ibase_prepare($dbh, $sql);

            foreach ($produtos as $produto){
                $resultado = ibase_execute($statement, 
                    $produto->getReferencial(),
                    $produto->getRefSaida(),
                    $produto->getNome(),
                    $produto->getUnitario(),
                    $produto->getQuantidade(),
                    $produto->getValor());

                if ($resultado == false)
                    $status = false;
            }
            ibase_free_query($statement);
            return $status;
            
        }
    }
?>