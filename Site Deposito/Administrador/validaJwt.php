<?php

    require_once('Banco/AdmDAO.php');
    require_once('Atualizador.php');
    
    $adm = new AdmDAO;
    $pswd = ''; //armazena a senha
    $token = $_POST['token']; // recupera o token do post
    
    //---Trecho responsável pela validação do token
    $part = explode(".", $token); // separa as partes do token
    $header = $part[0]; // captura o header
    $payload = $part[1]; // captura o payload
    $signature = $part[2]; // captura a signature

    $info = json_decode(base64_decode($payload));

    //var_dump($info);

    $pswd = $adm->consultaPswd($info->email, $info->sub);

    //validação do token
    //é passado a senha cadastrada para gerar um token de validação
    $valid = hash_hmac('sha256', "$header.$payload", $pswd, true);
    $valid = base64_encode($valid);


    $resposta = new StdClass();

    $resposta->mensagem = "Entrou na validaJwt !";
        $resposta->signature = "{$signature}";
        $resposta->signatureValida = "{$valid}";
        $resposta->status = false;
        $resposta->statusBanco = false;

    //se o token recebido for iguaal ao token gerado, faz a ação
    if ($signature == $valid){

        
        $resposta->mensagem = "Token valido entrou na funcao!";
        $resposta->signature = "{$signature}";
        $resposta->signatureValida = "{$valid}";
        $resposta->status = true;
        $resposta->statusBanco = false;

        $atu = new Atualizador();
        $atualizouClientes = $atu->atualizarClientes();
        
        

        if ($atualizouClientes){
            $resposta->mensagem = "Token valido e atualizado clientes !";
            $atualizouReceber = $atu->atualizarReceber();
            if ($atualizouReceber){
                $resposta->mensagem = "Token valido e atualizado clientes e receber!";
                $atualizouProdutos = $atu->atualizarProdutos();
                if ($atualizouProdutos){
                    $resposta->mensagem = "Token valido e atualizado clientes, receber e produtos!";
                    $resposta->statusBanco = true;
                }else{
                    $resposta->mensagem = "Token valido e erro ao atualizar Produtos!";
                }
            }else{
                $resposta->mensagem = "Token valido e erro ao atualizar Receber!";
            }
            
        }   
        else{
            $resposta->mensagem = "Token valido, porem, erro ao atualizar Banco !";
            $resposta->statusBanco = false;
        }
            
    }else{
        $resposta->mensagem = "Token invalido !";
        $resposta->signature = "{$signature}";
        $resposta->signatureValida = "{$valid}";
        $resposta->status = false;
        $resposta->statusBanco = false;
    }

    echo json_encode($resposta);
?>