<?php
    require_once 'Arquivo.php';
    require_once 'Banco/ClienteDAO.php';
    require_once 'Banco/ReceberDAO.php';
    require_once 'Banco/ProdutoDAO.php';
 
    Class Atualizador{

        public static function atualizarClientes(){
            $arquivo = new Arquivo;
            $clienteDAO = new ClienteDAO;

            $clientes = $arquivo->lerCsvClientes(); 

            $apagouClientes = $clienteDAO->deleteClientes();
            $inseriuClientes = $clienteDAO->insertClientes($clientes);

            if ($apagouClientes && $inseriuClientes)
                return true;
            else
                return false;            
        }

        public static function atualizarReceber(){
            $arquivo = new Arquivo;
            $receberDAO = new ReceberDAO;

            $receber = $arquivo->lerCsvReceber();

            $apagouReceber = $receberDAO->deleteReceber();
            $inseriuReceber = $receberDAO->insertReceber($receber);

            if ($apagouReceber && $inseriuReceber)
                return true;
            else
                return false;
            
        }

        public static function atualizarProdutos(){
            $arquivo = new Arquivo;
            $produtoDAO = new ProdutoDAO;

            $produtos = $arquivo->lerCsvProdutos();

            $apagouProdutos = $produtoDAO->deleteProdutos();
            $inseriuProdutos = $produtoDAO->insertProdutos($produtos);

            if ($apagouProdutos && $inseriuProdutos)
                return true;
            else
                return false;
        }
    }
?>