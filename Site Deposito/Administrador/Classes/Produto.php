<?php
    Class Produto {
        private $referencial;
        private $ref_saida;
        private $nome;
        private $unitario;
        private $quantidade;
        private $valor;

        public function getReferencial(){
            return $this->referencial;
        }
        public function getRefSaida(){
            return $this->ref_saida;
        }
        public function getNome(){
            return $this->nome;
        }
        public function getUnitario(){
            return $this->unitario;
        }
        public function getQuantidade(){
            return $this->quantidade;
        }
        public function getValor(){
            return $this->valor;
        }

        public function setReferencial($referencial){
            $this->referencial = $referencial;
        }
        public function setRefSaida($ref_saida){
            $this->ref_saida = $ref_saida;
        }
        public function setNome($nome){
            $this->nome = $nome;
        }
        public function setUnitario($unitario){
            $this->unitario = $unitario;
        }
        public function setQuantidade($quantidade){
            $this->quantidade = $quantidade;
        }
        public function setValor($valor){
            $this->valor = $valor;
        }
    }
?>