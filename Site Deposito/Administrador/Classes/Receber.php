<?php
    Class Receber{
        private $referencial;
        private $ref_cliente;
        private $ref_saida;
        private $data;
        private $valor;

        public function getReferencial(){
            return $this->referencial;
        }
        public function getRefCliente(){
            return $this->ref_cliente;
        }
        public function getRefSaida(){
            return $this->ref_saida;
        }    
        public function getData(){
            return $this->data;
        } 
        public function getValor(){
            return $this->valor;
        }

        public function setReferencial($referencial){
            $this->referencial = $referencial;
        }
        public function setRefCliente($ref_cliente){
            $this->ref_cliente = $ref_cliente;
        }
        public function setRefSaida($ref_saida){
            $this->ref_saida = $ref_saida;
        }
        public function setData($data){
            $this->data = $data;
        }
        public function setValor($valor){
            $this->valor = $valor;
        }
    }
?>