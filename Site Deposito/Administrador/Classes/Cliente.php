<?php
    Class Cliente{
        private $referencial;
        private $razaoSocial;
        private $senha;

        public function getReferencial(){
            return $this->referencial;
        }
        public function getRazaoSocial(){
            return $this->razaoSocial;
        }
        public function getSenha(){
            return $this->senha;    
        }

        public function setReferencial($referencial){
            $this->referencial = $referencial;
        }
        public function setRazaoSocial($razaoSocial){
            $this->razaoSocial = $razaoSocial;
        }
        public function setSenha($senha){
            $this->senha = $senha;
        }
    }
?>