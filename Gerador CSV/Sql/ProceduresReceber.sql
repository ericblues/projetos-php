CREATE PROCEDURE CRIAR_TB_ARQRECEBER
AS  
    declare variable sqltabela VARCHAR(1000);
    declare variable caminho VARCHAR(200);
BEGIN
    caminho = 'C:\Users\Flavia\Desktop\sql\DadosExportados\Receber.csv';

    sqltabela = '';


    sqltabela = '
        CREATE TABLE ARQRECEBER EXTERNAL FILE '||ASCII_CHAR(39)||''||:caminho||''|| ASCII_CHAR(39)||'(
            REFERENCIAL CHAR(10) NOT NULL,
            SEP1 CHAR(1),
            REF_CLIENTE CHAR(10),
            SEP2 CHAR(1),
            REF_SAIDA CHAR(10),
            SEP3 CHAR(1),
            DATA CHAR(50),
            SEP4 CHAR(1),
            VALOR CHAR(50),
            SEP5 CHAR(1),
            CRLF CHAR(2));
    ';

    EXECUTE STATEMENT sqltabela;
END;

CREATE PROCEDURE PREENCHER_CSV_RECEBER
AS
    declare variable sqlInsert VARCHAR(1000);
BEGIN
    sqlInsert = '
        INSERT INTO ARQRECEBER
        SELECT
            REFERENCIAL, '||ASCII_CHAR(39)||';'||ASCII_CHAR(39)||',
            REF_CLIENTE, '||ASCII_CHAR(39)||';'||ASCII_CHAR(39)||',
            REF_SAIDA, '||ASCII_CHAR(39)||';'||ASCII_CHAR(39)||',
            DATA, '||ASCII_CHAR(39)||';'||ASCII_CHAR(39)||',
            VALOR, '||ASCII_CHAR(39)||';'||ASCII_CHAR(39)||',
            ASCII_CHAR(13) || ASCII_CHAR(10)
        FROM FIN_RECEBER
        WHERE STATUS IS NULL AND VALOR_RECEBIDO IS NULL
        AND REF_CLIENTE IN (
            SELECT REFERENCIAL FROM FIN_CLIENTES WHERE SENHA IS NOT NULL
        )
    ';

    EXECUTE STATEMENT sqlInsert;
END;

CREATE PROCEDURE APAGAR_TB_ARQRECEBER
AS
    declare variable sqlDrop VARCHAR(100);
BEGIN
    sqlDrop = 'DROP TABLE ARQRECEBER';
    EXECUTE STATEMENT sqlDrop;
END;