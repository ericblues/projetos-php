CREATE PROCEDURE CRIAR_TB_ARQPRODUTOS
AS
    declare variable sqltabela VARCHAR(1000);
    declare variable caminho VARCHAR(100);
BEGIN

    caminho = 'C:\Users\Flavia\Desktop\sql\DadosExportados\Produtos.csv'; 

    sqltabela = '
        CREATE TABLE ARQPRODUTOS EXTERNAL FILE '||ASCII_CHAR(39)||''||:caminho||''|| ASCII_CHAR(39)||' (
            REFERENCIAL CHAR(10) NOT NULL,
            SEP1 CHAR(1),
            REF_SAIDA CHAR(10),
            SEP2 CHAR(1),
            NOME CHAR(100),
            SEP3 CHAR(1),
            UNITARIO CHAR(6),
            SEP4 CHAR(1),
            QUANTIDADE CHAR(10),
            SEP5 CHAR(1),
            VALOR CHAR(10),
            SEP6 CHAR(1),
            CRLF CHAR(2)
        );
    ';

    EXECUTE STATEMENT sqltabela;
END;

CREATE PROCEDURE PREENCHER_CSV_PRODUTOS
AS
    declare variable sqlInsert VARCHAR(1000);
BEGIN

    sqlInsert = '
        INSERT INTO ARQPRODUTOS
            SELECT REFERENCIAL, '||ASCII_CHAR(39)||';'||ASCII_CHAR(39)||',
            REF_SAIDA, '||ASCII_CHAR(39)||';'||ASCII_CHAR(39)||',
            NOME, '||ASCII_CHAR(39)||';'||ASCII_CHAR(39)||',
            UNITARIO, '||ASCII_CHAR(39)||';'||ASCII_CHAR(39)||',
            QUANTIDADE, '||ASCII_CHAR(39)||';'||ASCII_CHAR(39)||',
            VALOR, '||ASCII_CHAR(39)||';'||ASCII_CHAR(39)||',
            ASCII_CHAR(13) || ASCII_CHAR(10) FROM VEN_SAIDA_PRODUTOS
            WHERE REF_SAIDA IN (
                SELECT
                    REF_SAIDA
                FROM FIN_RECEBER
                WHERE STATUS IS NULL AND VALOR_RECEBIDO IS NULL
                AND REF_CLIENTE IN (
                    SELECT REFERENCIAL FROM FIN_CLIENTES WHERE SENHA IS NOT NULL
                )
            );
    ';

    EXECUTE STATEMENT sqlInsert;
END;

CREATE PROCEDURE APAGAR_TB_ARQPRODUTOS
AS
    declare variable sqlDrop VARCHAR(100);
BEGIN
    sqlDrop = 'DROP TABLE ARQPRODUTOS';

    EXECUTE STATEMENT sqlDrop;
END;