--Consulta na tabela FIN_CLIENTES onde 
--a senha seja diferente de null
--e carrega os dados da consulta no arquivo csv


--Procedure responsável por criar a tabela temporária ARQCLIENTES
--Que cria uma referência para o arquivo Clientes.csv no caminho especificado
--Obs: ASCII_CHAR(39) = '
CREATE PROCEDURE CRIAR_TB_ARQCLIENTES
AS
    declare variable sqltabela VARCHAR(1000);
    declare variable caminho VARCHAR(200);
BEGIN

    caminho = 'C:\Users\Flavia\Desktop\sql\DadosExportados\Clientes.csv';

    sqltabela = '
        CREATE TABLE ARQCLIENTES EXTERNAL FILE '||ASCII_CHAR(39)||''||:caminho||''|| ASCII_CHAR(39)||'(
            REFERENCIAL CHAR(6) NOT NULL,
            SEP1 CHAR(1),
            RAZAO_SOCIAL CHAR(100),
            SEP2 CHAR(1),
            SENHA CHAR(6),
            SEP3 CHAR(1),
            CRLF CHAR(2));
    ';

    EXECUTE STATEMENT sqltabela;
END;


--Procedure responsável por preencher o arquivo Clientes.csv
--Nesse ponto o arquivo é definitivamente criado no caminho especificado
--Os daados do arquivo são o retorno da consulta feita dentro do INSERT
--Obs: ASCII_CHAR(13) || ASCII_CHAR(10) representam o caractere CR/LF
--que no Windows representa o fim de uma linha em um arquivo
CREATE PROCEDURE PREENCHER_CSV_CLIENTES
AS
    declare variable sqlInsert VARCHAR(1000);
BEGIN

    sqlInsert = '
        INSERT INTO ARQCLIENTES
            SELECT REFERENCIAL, '||ASCII_CHAR(39)||';'||ASCII_CHAR(39)||',
            RAZAO_SOCIAL, '||ASCII_CHAR(39)||';'||ASCII_CHAR(39)||',
            SENHA, '||ASCII_CHAR(39)||';'||ASCII_CHAR(39)||',
            ASCII_CHAR(13) || ASCII_CHAR(10)
            FROM FIN_CLIENTES WHERE SENHA IS NOT NULL ORDER BY REFERENCIAL;
    ';

    EXECUTE STATEMENT sqlInsert;
END;

--Procedure responsável por limpar todos os registros
--na tabela FIN_CLIENTES
CREATE PROCEDURE LIMPAR_FIN_CLIENTES
AS
    declare variable sqlDelete VARCHAR(250);
BEGIN
    sqlDelete = 'DELETE FROM FIN_CLIENTES';
    EXECUTE STATEMENT sqlDelete;
END;

--Procedure responsável por preenche a tabela FIN_CLIENTES
--com os dados do CSV
CREATE PROCEDURE PREENCHER_FIN_CLIENTES
AS
    declare variable sqlInsert VARCHAR(1000);
BEGIN
    sqlInsert = '
        INSERT INTO FIN_CLIENTES (REFERENCIAL, RAZAO_SOCIAL, SENHA)
            SELECT 
                REFERENCIAL,
                RAZAO_SOCIAL,
                SENHA 
            FROM ARQCLIENTES ORDER BY REFERENCIAL;';

    EXECUTE STATEMENT sqlInsert;
END;


--Procedure responsável por apagar a tabela temporária ARQCLIENTES
--Obs: Apagando a tabela temporária, o arquivo csv é mantido
--Apenas a tabela é apagada do banco, mas os dados já estão salvos
CREATE PROCEDURE APAGAR_TB_ARQCLIENTES
AS
    declare variable sqlDrop VARCHAR (100);
BEGIN
    sqlDrop = 'DROP TABLE ARQCLIENTES';

    EXECUTE STATEMENT sqlDrop;
END;
