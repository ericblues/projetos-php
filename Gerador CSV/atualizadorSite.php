<?php

    require_once('Client/Requisicao.php');
    require_once('Client/conexao.php');

    function uploadArquivo($caminho, $nomeArquivo){

        //status representa se foi feito o upload do arquivo
        $status = false;

        //Dados da conexão ftp
        $ftp_server = "";
        $ftp_user = "";
        $ftp_pswd = "";

        $file = $caminho.$nomeArquivo;
        $remote_file = "/www/CSV/".$nomeArquivo;

        //abrindo a conexão
        $conn_id = ftp_connect($ftp_server);

        //login com usuario e senha
        $login_result = ftp_login($conn_id, $ftp_user, $ftp_pswd);

        // Ativando modo passivo
        ftp_pasv($conn_id, true);

        echo "\n\n Carregando arquivo $nomeArquivo, aguarde ... \n";

        //resultado da transferencia
        //inicia o upload do arquivo
        $ret = ftp_nb_put($conn_id, $remote_file, $file, FTP_BINARY);

        //enquanto a tranferencia nao estiver concluida, continua o processo de upload
        while ($ret == FTP_MOREDATA) {
            
            echo " .";
            // Continue uploading...
            $ret = ftp_nb_continue($conn_id);
        }
        //Se a tranferencia for diferente de finalizado
        //Houve algum erro no upload do arquivo
        if ($ret != FTP_FINISHED){
            $status = false;
        }else{
            $status = true;
        }
           

        //fecha a conexao ftp
        ftp_close($conn_id);

        return $status;
    }

    function limparArquivos(){
        $clientes = 'C:\Users\Flavia\Desktop\sql\DadosExportados\Clientes.csv';
        $receber = 'C:\Users\Flavia\Desktop\sql\DadosExportados\Receber.csv';
        $produtos = 'C:\Users\Flavia\Desktop\sql\DadosExportados\Produtos.csv';

        echo "\n Verificando arquivo Clientes.csv \n";
        if (file_exists($clientes)){ //Verifica se o arquivo existe
            echo "\n  Limpando arquivo Clientes.csv \n\n";
            $file = fopen($clientes, "r+"); //Abre o arquivo para edição
            ftruncate($file, 0); //Zera o arquivo
            rewind($file); //Seta o ponteiro na posição inicial do arquivo
            fclose($file); //Fecha o arquivo
        }
        echo "\n Verificando arquivo Receber.csv \n";
        if (file_exists($receber)){
            echo "\n  Limpando arquivo Receber.csv \n\n";
            $file = fopen($receber, "r+");
            ftruncate($file, 0);
            rewind($file);
            fclose($file);
        }
        echo "\n Verificando arquivo Produtos.csv \n";
        if (file_exists($produtos)){
            echo "\n  Limpando arquivo Produtos.csv \n\n";
            $file = fopen($produtos, "r+");
            ftruncate($file, 0);
            rewind($file);
            fclose($file);
        }
    }
    function gerar_csv($opcao){

        switch ($opcao) {
            case 1:
                $sqlCriar = 'EXECUTE PROCEDURE CRIAR_TB_ARQCLIENTES';                    
                $sqlPreencher = 'EXECUTE PROCEDURE PREENCHER_CSV_CLIENTES';          
                $sqlApagar = 'EXECUTE PROCEDURE APAGAR_TB_ARQCLIENTES';
                $nomeCsv = 'CLIENTES.CSV';
                break;
            case 2:
                $sqlCriar = 'EXECUTE PROCEDURE CRIAR_TB_ARQRECEBER';                    
                $sqlPreencher = 'EXECUTE PROCEDURE PREENCHER_CSV_RECEBER';          
                $sqlApagar = 'EXECUTE PROCEDURE APAGAR_TB_ARQRECEBER';
                $nomeCsv = 'RECEBER.CSV';    
                break;
            case 3:
                $sqlCriar = 'EXECUTE PROCEDURE CRIAR_TB_ARQPRODUTOS';                    
                $sqlPreencher = 'EXECUTE PROCEDURE PREENCHER_CSV_PRODUTOS';          
                $sqlApagar = 'EXECUTE PROCEDURE APAGAR_TB_ARQPRODUTOS';
                $nomeCsv = 'PRODUTOS.CSV';    
             break;
            default:
                echo "Erro ao gerar csv";
                break;
        }               
                                                                            
        $criarTabela = ibase_prepare($sqlCriar);
        
        echo "\n\n Preenchendo arquivo $nomeCsv!";
        echo "\n Aguarde ...";
        if (ibase_execute($criarTabela)){
            ibase_commit();
            $preencherCSV = ibase_prepare($sqlPreencher);
            if (ibase_execute($preencherCSV)){
                ibase_commit();
                $apagarTabela = ibase_prepare($sqlApagar);
                ibase_execute($apagarTabela);
                ibase_commit();
            }
        }
    }

    echo "\nProcessando Backup dos dados para o site, aguarde ... \n";

    try{
        
        //limpar csv antigo e gerar novos
        limparArquivos();

        //gerando os csvs
        $i = 1;
        while ($i <= 3) {
            gerar_csv($i);
            $i++;
        }

        $caminho = 'C:\Users\Flavia\Desktop\sql\DadosExportados';

        //realizar upload dos arquivos gerados
        $subiuCliente = uploadArquivo($caminho, "\CLIENTES.CSV");   
        $subiuReceber = uploadArquivo($caminho, "\RECEBER.CSV");  
        $subiuProdutos = uploadArquivo($caminho, "\PRODUTOS.CSV");

        if ($subiuCliente && $subiuReceber && $subiuProdutos){
            $req = new Requisicao();
        
            if ($req->atualizarBancoSite()){
                echo "\n\n Banco de dados atualizado com sucesso!";
            }else{
                echo "\n\n Houve uma falha ao atualizar o banco de dados ...";
            }
        }

    }catch(Exception $e){
        echo "Ocorreu um erro: ", $e->getMessage(), "\n";
        readline();
    }finally{
        echo "\n\nTecle <Enter> para encerrar o programa ... ";
        readline();
        echo "\n\nBackup Finalizado.";
    }
?>
