<?php
    require_once 'HTTPRequester.php';

    Class Requisicao{

        public static function atualizarBancoSite(){

            //status diz se o site foi atualizado ou n�o
            $status = false;

            $pswd = 'minhasenha';

            $header = [
                'alg' => 'HS256',
                'typ' => 'JWT'
            ];
            $header = json_encode($header);
            $header = base64_encode($header);

            $payload = [
                'iss' => 'localhost',
                'sub' => '1',
                'name' => 'administrador',
                'email' => 'exemplo@email.com.br'
            ];
            $payload = json_encode($payload);
            $payload = base64_encode($payload);

            $signature = hash_hmac('sha256', "$header.$payload", $pswd, true);
            $signature = base64_encode($signature);

            $url = '';
            //$url = 'http://localhost/PhpExec/Server/validaJwt.php';

            //echo $header.$payload.$signature;
            $response = HTTPRequester::HTTPPost($url,
                        array('token' => "{$header}.{$payload}.{$signature}"));

            //echo $response;
            //var_dump($response);

            $resposta = json_decode($response);
            
            echo "\n\nMensagem.......................: {$resposta->mensagem}";
            echo "\n\nAssinatura Enviada.............: {$resposta->signature}";
            echo "\n\nAssinatura Valida..............: {$resposta->signatureValida}";
            echo "\n\nStatus de atualizacao do Banco.: {$resposta->statusBanco}";

            return $resposta->statusBanco;
        }
    }

?>
