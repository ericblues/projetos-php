<?php
    $pswd = 'minhasenha';

    $header = [
        'alg' => 'HS256',
        'typ' => 'JWT'
    ];
    $header = json_encode($header);
    $header = base64_encode($header);

    $payload = [
        'iss' => 'localhost',
        'sub' => '1',
        'name' => 'administrador',
        'email' => 'exemplo@email.com.br'
    ];
    $payload = json_encode($payload);
    $payload = base64_encode($payload);

    $signature = hash_hmac('sha256', "$header.$payload", $pswd, true);
    $signature = base64_encode($signature);

    echo "$header.$payload.$signature";
?>
